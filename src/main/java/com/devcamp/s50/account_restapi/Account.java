package com.devcamp.s50.account_restapi;

public class Account {
    private int id;
    private String name;
    private int balance = 0;

    public Account(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Account(int id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }
    public int credit(int amount){
        this.balance += amount;
        return this.balance;
    }
    public int debit(int amount) {
        if(amount <= this.balance){
            this.balance -= amount;
        } else {
            System.out.println("Amount exeeded balnce");
        }
        return this.balance;
    }

    @Override
    public String toString() {
        return "Account [balance=" + balance + ", id=" + id + ", name=" + name + "]";
    } 
}
