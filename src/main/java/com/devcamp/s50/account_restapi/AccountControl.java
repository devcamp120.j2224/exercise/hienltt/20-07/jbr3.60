package com.devcamp.s50.account_restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountControl {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList <Account> getListAccount(){
        Account account1 = new Account(11, "Nguyen Van AA", 500000);
        Account account2 = new Account(22, "Nguyen Van CC", 1000000);
        Account account3 = new Account(33, "Nguyen Van EE", 999999);
        System.out.println(account1);
        System.out.println(account2);
        System.out.println(account3);

        ArrayList <Account> listAccount = new ArrayList<>();
        listAccount.add(account1);
        listAccount.add(account2);
        listAccount.add(account3);
        return listAccount;
    }
}
