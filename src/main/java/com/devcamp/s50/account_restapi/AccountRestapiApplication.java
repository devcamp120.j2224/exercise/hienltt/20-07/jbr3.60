package com.devcamp.s50.account_restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountRestapiApplication.class, args);
	}

}
